# CHANGELOG

- 20220414	

		this is a cherrypicked CHANGELOG add as there are loads of changes due to different 
		audio transport ideas etc. For a full changelog exec: git log"

    		About box updated to include Patreon site 

    		added 500 Hz step for VFO

		Added clock and UTC toggle.

    		Added forced manual RTS setting

    		Add RIT function and other rigctl fixes

    		Adjusted window size for radios without spectrum. Thanks K5TUX.

    		Allow dynamic restarting of server

    		New Settings tab

    		Enable High DPI Scaling

    		More multi-radio support

		many rigctl improvements/cmpat adds

		N1MM+ TCP connection added
    
- 20211201

    		Another "minor" update for RX only rigs

    		Disable certain TX commands for RX only rigs

- 20211222

    		Add mutex within rigState to protect access

- 20211119

    		Add more version info

    		Add --version command line argument and WFVIEW_VERSION #define

- 20211118

    		A little less green in our gray.

    		Added size rules for audio source combo boxes.

    		Fix silly bug in retransmit code

    		Remove some extra logging from audio

- 20211115

    		Add mutex for incoming audio on udp and server

    		Force PA to use 48K Sample Rate if default is 44.1K

    		Try using slot for audio again

- 20211110

    		Server only tries mutex lock for 10ms before giving up.

    		Check number of samples in opus packet before attempting decode

- 20211109

    		Small changes to audio handler

- 20211107

    		Add SSE2 enhancements to resampler

    		Update audiohandler.h

    		Enable SSE or NEON enhancements for resampler

    		Fix resample ratio for input and output (hopefully!)

    		Create resampleRatio

    		Close PA stream after stopping it!

    		Fix Linux rtaudio build

    		Pulled out some debug code that isn't needed.

    		Fixed manual rig ID issue with uninitialized variable.

- 20211106

    		Added override allowing the user-specified CI-V address to also be used
    		as the Rig ID (model). This is useful for older radios that do not reply
    		to Rig ID queries. For radios using the default CI-V address, it should
    		"just work".

    		Added PTT "snooping" to the commHandler, such that radios needing RTS
    		set for PTT will work. Includes replying to PTT queries with the RTS
    		status. There is currently no UI to manually select radios that need
    		RTS.

    		deleted a lingering swapfile from one of my vim sessions

- 20211105

    		Added RTS PTT support commHandler and rigCommander. RTS is only sent
    		from rigCommander at this time, the pty is not parsed.

    		Added geometry constraints to the transceiver adjustments window, and
    		disable controls which do not function except for debug builds.


    		Changed IF/TPBF commands to be "unique priority" inserts. Added "more"
    		button for extended transceiver controls.

    		Added an IF Shift-like control for radios with Twin PBF.

    		Added support for IF Shift and Twin Pass-Band Filters. Currently
    		accessible only via the debug button.

- 20211104
    		Added IC-736 FM mode

    		Added code to force IC-736 to rigID

- 20211101

    		Use QT Audio by default

    		remove unneeded audio signal handler

    		Add portaudio support

    		Make switching between audio apis easier (and tidy .pro file)

    		Use buffered audio for Linux (was just Mac only)

    		Adjust buffer size depending on latency setting

- 20211031

    		Stuff audio buffer directly rather than signal/slot

- 20211022

    		Don't block until audio buffer has space

    		Bit of tidying

- 20211020

    		Tidy-up server shutdown

    		Trying to find cause of lockup when client disappears

- 20211006

    		Send TX/Freq changes multiple times with rigctld

		bumped to 1.2d hopefully last testversion before 1.20
- 202109022

    		Remove duplicate setPriority()

    		Fix typo

		Add keepalive for linux/mac pty

    		Fix alignment of rigname in taskbar

    		Only send RX antenna byte to rig when it has an RX antenna option in rigCaps


- 20210907

	        Make rigctld state work for USB connected rigs

- 20210831

    		added 25 kHz step for tuning

- 20210829

    		Experimental support for split mode in rigctld

    		Ignore control levels that we don't currently support

    		Add better detection of ci-v transceive disable

- 20210827

    		Add saving of meter2 state

- 20210823

    		Set audio thread priority in the correct place!

    		Now with dual meters for everyone!

- 20210820

    		Clear Center readout as well.

    		Fixed issue where the "none" selection didn't work quite right. Also
    		fixed the T/R meter switching to clear out invalid readings.


    		Set audio threads to be realtime priority

- 20210817

    		dual meter support/WHATSNEW

    		Current scale tic marks on both sides now!

    		More meter work, fixed some scales and added labels.

    		Better meter fonts

- 20210815

    		Fix for left over CIV client on server

    		Improve detection of unsupported codec.

- 20210814

    		Fix for Opus TX audio

    		txrate logging wrong

    		More warning removal!

    		More commenting in rigctld.h

    		Is this going to fix 8 bit audio?

    		Comment unused structs from rigctld

    		All audio codecs now work in both directions!

		Update audiohandler.cpp

		more 8bit fixes!

		ulaw encoding test

		8 bit encoding fix

		Another issue with  8 bit audio

		Try again to remove debugging!

		Revert "Remove extra opus debugging"  This reverts commit da53f5371bbeb35b10cbb831b83b74e1bdd771f9.

		Remove extra opus debugging

		Move opus init

		Add some debugging for decoder

		Use radio samplerate for opus

		Remove big/little endian conversion

		More Opus fixes

		Update audiohandler.cpp

		Try to fix Opus 2-channel

- 20210809

    		Add constants to make parsing (hopefully) easier

- 20210808

    		Fake known functions

    		Fix float warning

    		Remove calibration debugging

    		Add proper s-meter calibration

- 20210807

    		Add ritctl model to rigCaps

    		Fix to make wsjt-x work again!

    		Add split/duplex support

    		Update rigctld.cpp

    		Correct lack of parentheses in conditionals

    		Fix typo

    		Remove some debug logging

    		More rigctl features/fixes

- 20210806

    		Fix for get_powerstat

    		Update rigctld.cpp

    		Add some levels and other functions

    		Fix compile warnings

    		Add frequency ranges from rigcaps

- 20210806

    		Move rigctld settings in Ui

    		Fixes for setting freq/mode

    		Support for more rigctld commands

    		More fixes to rigctld

    		Change the way rigctl response is built

    		More rigctld fixes

- 20210804

    		Add rigctld config to ui and fix some bugs

- 20210802

    		added derSuessman prefix code

- 20210801

    		Fix broken 8bit audio

    		added derSuessmann additions to have a linux install prefix

    		Fedora build/install notes, see merge request eliggett/wfview!4

- 20210730

    		Added a little extra logic, also some cross-platform help, to the custom
    		stylesheet loader.

- 20210729

    		fix: set the style once


    		added /usr/local to search path for the stylesheet

- 20210726

    		Fixed error in IC-7410 attenuator spec.

- 20210726

    		Fix for blank username/password in server

- 20210724

    		small changes to INSTAll.md and addition of mint 20.2/openSUSE 15.3

- 20210720

    		Clear out meter values when type is switched.

    		Allow user to turn off power-down confirmation msgbox

- 20210719

    		wfview now uses the meter's own balistics (average and peak code). This
    		makes it very easy to meter any parameter 0-255. Meter Type "meterNone"
    		or other will display data in "raw" format.

- 20210718

    		Added center tuning for IC-R8600, partially moved meter balistics
    		(average and peak) to the meter class.

    		Quick debug for the metering queue, just in case.

 20210717

    		Preliminary secondary meter support. See Settings tab for selection.
    		Some scales incomplete.

    		Added SWR and ALC scales to the meter.

    		Fix error in scale of power meter

- 20210716

    		Power meter for transmit. Much work remains on this meter alone.

    		Get antenna status on start-up and slow poll for it.

    		Add RX antenna selection for rigs that support it

- 20210714

    		Preferences added for Anti-Alias and Interpolate.

- 20210713

    		Added waterfall display options: anti-alias and interpolate. Not in
    		preferences yet. Debug button enables wf pan and zoom.

- 20210711

    		Allow user to select whether to confirm exit or not

- 20210709

    		Reset PTT timer for control-R keystroke.

    		Added a fix to keep the local frequency in-sync with any recent commands
    		sent to the radio.

    		Added more support for the IC-7600 in rigCaps.

    		Added time, date, and UTC offset commands. Currently initiated by the
    		debug button. There seems to be a bug in the 7300 where the UTC offset
    		has one hour subtracted, ie, -7 HRS becomes -8 HRS. The hex command
    		appears to be sent correctly.

- 20210708

    		Delete oldest entry from UDP buffer before adding new one.

    		If pty connected s/w sends a disable transceive command, dont' send any transceive commands to it

    		New about box!


- 20210706

    		Local af gain now has anti-log audio pot taper.

    		Only start audio when stream is ready

- 20210705

    		Added transceiver adjustment window show code, for the debug button only
    		currently.

    		Added window title text change to show radio model. Needs to be checked
    		cross-platform. On Linux Mint, displays: "IC-7300 -- wfview"

    		Applying setup.volume within audio handler if the audio handled is an
    		output. Added this to the init function.

    		waterfall theme is now saved.

    		Added local af gain and wf length to the preferences.

- 20210702
		fixed small error where the tx latency was not copied in the UI
		
- 20210626
    		Merge branch 'sequence' of gitlab.com:eliggett/wfview into sequence

    		Duplicate of existing command.

    		Remove unnecessary escape sequence

    		Fix for fix of missing __PRETTY_FUNCTION__

    		Add __PRETTY_FUNCTION__ for compilers that don't have it.

- 20210625
    		Mode changes from the combo box now use the que. There are still other
    		methods to change mode which will transition shortly.

    		Faster PTT

    		Added PTT to the queue.

    		Added unique priority insertion methods.

    		Changed how commands with parameter data are added.

    		Initial queued "set" command commit. Only the frequency set command is
    		used so far, and only for the "Frequency" tab and the tuning knob.

- 20210624
    		Quick hack for WFM forcing FIL1 always

- 20210621
    		Added polling button

    		Moving to std::deque (double-ended que).

- 20210620

    		IC-R8600 span is now received into the UI correctly.

    		New unified outgoing command queue. Tested on IC-9700 and IC-718 (to
    		remote wfview server). CPU usage seems higher but please check your
    		system.
    
    		Timing seems to be acceptable but could probably use some tweaks. 
		S-meter polling is 25ms for fast radios, and slower rates for slower
    		radios. Half-duplex serial radios receive 3x slower polling to make room
    		for replies.
    
    		For Freq, Mode, etc "regular" constant polling (new feature):
    
    		IC-9700 polling is 5 per second, IC-718 is 1-2 per second.
    
    		Just helps keep the UI in sync with changes taking place at the rig. The
    		polling is slow enough that it doesn't impact anything. But quick enough
    		that it catches discrepancies pretty quickly.

- 20210619

    		Added a few more slider things

    		whatsnew: improved IC-R8600

- 20210618

    		Additional support for the IC-R8600, including wider scope spans.

    		Minor change to remove some old debug code that snuck in.

    		If no rig caps, then don't mess with the window!

    		Added full duplex comms parameter to rigCaps. We assume half-duplex
    		until we receive a reply to rigID.

    		Fixed accidental s-meter timing parameter change.

- 20210617

    		Radios without spectrum do not show spectrum, and, the window properly
    		resizes for those controls. Also, a new key command, control-shift-d has
    		been added to run debug functions from any tab in the program.

- 20210615

    		Additional code to hide/show spectrum and correcting an issue with the
    		rig name not populating for non-spectrum radios.

    		Dynamic show/hide spectrum for rigs without this feature.

    		Additional data corruption checking.

- 20210614

    		Changed collision detection code so that we can more easily see what
    		message was missed.

    		Added collision detection for serial commands. Collisions are apparently
    		frequent for true 1-wire CI-V radios.

    		We now calculate polling rates immediately upon receiveCommReady for
    		serial connections. For network connections, we assume sane values and
    		modify once we receive the baud rate from the server.

    		Add Neon (ARM) support to resampler

    		Revert to using resampler directory rather than opus-tools submodule

- 20210612

    		Add tooltip showing percentage of TX power when slider is moved

- 20210611

                adding a second path/way for the qcustomplot link if the first fails

    		Update udpserver.cpp

    		Use global watchdog rather than per-connection

    		Report when users are disconnected by the watchdog

    		Use watchdog to cleanup lost server connections

    		Fix crash on disconnect

    		Make status update after disconnection

    		More server disconnection cleanup

    		Improve server disconnection/cleanup

- 20210610

    		remove spaces when adding extra server users

    		Update udpserver.cpp

    		Allow both encoded and plain text passwords

    		Lots of fixes to server setup table

    		Hopefully fix the occasional 0xe1 packet from hitting the pty

    		Add more info for server connections

    		Make sure that user is authenticated before allowing CIV/Audio

    		Use correct location for statusupdate!

    		Indicate when TX is not available

    		Show server connection status in taskbar (only for USB connected rigs)

- 20210609

    		Allow sender or receiver to be 0xe1 in server

    		Always forward wfview traffic to wfview clients

- 20210608

    		Truncate wfview.log on open

    		Detect radio baudrate in server mode

    		Comment out rtaudio if not being used

    		Baud rate calculations are now only happening when baud rate is received
    		and reasonable.

- 20210607

    		Check that we have at least 1 audio channel available.

    		Improve audio cleanup

    		Add extra debugging for UDP server CIV

    		Make only MacOS use buffered audio

    		Improve mac audio

- 20210606

    		Fix TX Audio on Linux

    		Various fixes to udpserver

    		Make QTMultimedia default

    		Fix to allow rtaudio to compile again

- 20210605

    		Add latency check to TX audio

    		Fix incorrect use of latency setting

- 20210604

    		Stop silly compile warning

    		Change udpserver to use new audiosetup struct properly.

    		Fix audio device selection

    		Fix for txaudio

    		Add QtMultimedia as default audio

- 20210603

    		Hopefully fix hang on exit when trying to close audio thread.

- 20210602

    		Use heap based rtaudio for enumeration

    		Catch possible exception when closing non-existent stream

   		Fix mac audio

    		Fix mac crash on closing audio

    		Make sure audio is deleted in destructor

    		Force specific rtaudio API depending on platform.

    		Linux now uses librtuadio-dev where available

    		Removing local rtaudio code and using library instead.

    		revert to ALSA for now

    		Tell rtaudio not to hog device

    		Update audiohandler.cpp

    		Select chunksize based on native sample rate

    		Force 48K sample rate when 44100 is detected.

    		change linux compiler directive to allow new-aligned

    		Tidy up server mutexes

    		Fix server high CPU on disconnect

- 20210601

    		Stop deleting audio after last client disconnects

    		Change udpserver packet handling to be similar to udphandler

    		Update udpserver.cpp

    		Let buffer keep filling.

    		Change to 10ms poll time for server rx audio

    		Mutex work in udpserver

    		Fix for crash when remote requests tone.

- 20210531

    		IC-7700 support

    		Open pty non-blocking

    		Fix for crashing pty on mac

    		Fix compile issue after merge

- 20210530

    		Keep the theme during resize. TODO: preference for wf theme

    		Removing my own uninformed sidenote.

    		Waterfal length may now be adjusted. Let's see what range of length
    		seems good and limit the control accordingly. Also there may be a memory
    		leak in the prepareWf() function where the colormap is created when the
    		image is resized.

    		CIV may now be changed as-needed while running.

    		Remove various compiler warnings and tidy up.

    		add silence if audio has stopped

- 20210529

    		fix for mac/linux compiler

    		Detect number of device channels and convert accordingly

    		Lots more changes for rtaudio compatibility

    		Small change to show default audio devices

- 20210528

    		More chair movements.

    		More arranging of the chairs. Also fixed a minor bug that prevented the
    		"Manual" serial device entry on my system.

    		Cleaning up the main constructor for wfmain.

    		Add some startup logging

    		Update audiohandler.cpp

    		Update udphandler.cpp

    		Change toolbar display formatting

    		Use preferred sample rate rather than force 48000

- 20210527

    		Allow higher latency

    		udpserver fixes

    		Update udpserver.cpp

    		Fix for tx audio channels

    		Add tx audio

    		add asound lib to linux build

    		fix qmake file

    		Use ring buffer with rtaudio to eliminate mutexes

- 20210525

    		Update INSTALL_PREBUILT_BINARY.md

    		move ulaw to dedicated header

    		add rtaudio as submodule

    		add opus-tools as submodule

    		Add mutex for audio buffer

- 20210523

    		Fixes for linux build

    		First working rtaudio (output only)

    		Link can now be clicked.

    		Added helpful text to settings tab.

    		Allow entry to Server Setup for either radio connection type.

    		Minor change to clarify roll of Server Setup

- 20210522

    		Add debugging and fix silly error in audiooutput combobox

    		re-enable audio buffers

    		Attempt to fix crash

    		Make only first client have TX audio

    		Stop audiohandler re-enumerating devices on connect.

    		Stop preamps/attenuators lists growing every time we reconnect.

    		Try increasing audio device buffer size

- 20210521

    		Changed method for adding modes to rigs and populating the rig menu.
    		This should be easier to maintain and better in the long run.

    		"Hopefully" fix annoying UDP server crash on client disconnect!

    		Fix for TX audio in udp server

    		Fix for stuttering audio on mac

    		Fixed missing break in switches.

    		Typo in message about CI-V

    		Dynamic timing update for all baud rates and connection types.

    		Fixed support for 9600 baud and lower speeds.

- 20210521

    		Add baud rate detection for remote rigs

    		Correct propCIVAddr to work if less than 0xe0

    		Change audiohandler to use latency for tx audio


- 20210520

    		Added IC-756 Pro. Tested UI and back-end response with 7300 and fake
    		RigID reply.

    		Added additional support for the IC-756 Pro III. Essentially untested.

    		Cleaned up warning and UI help text.

    		Add more features to rigstate

- 20210519

    		Model ID text format fixed. Shows IC-0x followed by the raw rig ID
    		received.

    		Fixed issue where unknown rigs were identified as 0xff. All rigs are now
    		identified using rigCaps.modelID, populated with the raw response from
    		the Rig ID query. Enumerations of known rig types continue to fall into
    		rigCaps.model, and unknown rigs will match to rigCaps.model=modelUnknown,
    		as before.

    		Serial baud rate is in the UI now. Added some enable/disable code to
    		prevent confusion about which options can be used with which types of
    		connections.

    		Better about box.

    		fixed filename in instructions

		Add MacOS serial port entitlement


- 20210518

    		Remove unused variables

    		Make windows build include git version
    		Command line version of git must be available in the path.

    		Various file tidying for Windows/Mac builds

    		Flush buffers if too many lost packets.

    		remove duplicate audioPacket metatype

    		Fix silly error in udpserver audio

    		Allow receive only in udpHandler

    		Manual CIV is now read from the preferences and populates the UI
    		accordingly.

    		Changed UI a little, and added manual CI-V options. Seems to work well.

- 20210517

    		Fixes for MacOS build - sandbox

    		Fixes for mac logging/pty since adding sandbox

    		Make audio input buffer a qMap

    		Use qMap instead of qVector for buffers as they are auto-sorted.

- 20210516

    		Fixed attenuator on IC-7000

    		register audio metatype in wfmain with all of the others

    		Move manual serial port so linux/mac only

- 20210516 	Added IC-7200 support. This has not been tested.

- 20210515	Change to correct bug when CI-V is user-specified and rigCaps were never
    		populated.

    		Making the s-meter error command easier to read.

    		Added IC-706 to rigCaps. Support for the IC-706 is currently broken but
    		development is in progress.

    		Added check for if the rig has spectrum during initial state queries.

    		BSR debug code.

		Small fixes for rigctld

- 20210514	Make UDP port textboxes expanding.

    		Selecting an antenna now sets that antenna for TX and RX.
                
    		wfview now closes when the main window is closed.
                
    		Filter selection now checks for data mode.
                
    		Preliminary IC-7000 support. TODO: Input selection, modes, filters,
    		reported bug with frequency input and s-meter.
                
    		Moved the power buttons.

    		Cyan for the tuning line.

    		Resize UDP port text boxes (again!)

    		ake UDP port textboxes expanding.

    		Fixes to UDP server

		Hopefully improve stability of pty by filtering traffic for any other CIV id.

- 20210513	Additional search path for macports compatibility (macOS only).

    		Slower polling for older rigs using lower serial baud.

    		Fix CI-V packet length bug in udphandler

    		Set pty serial port to NULL by default

- 20210512	Fix for crash on MacOS

- 20210511	Fixes for virtual serial port in Windows.

    		Initial commit of pty rewrite
		There is a new combobox in Settings which shows the file that the pty device is mapped to. 
		You can select from ~/rig-pty[1-8] which should work if you have multiple instances of wfview.

- 20210509	Enhanced accessibility for the UI.

- 20210508	Data Mode now sends the currently selected filter.

    		Removed -march=native compiler and linker flags. This should make the
    		binary builds more compatible, and also eliminate an issue on Pop! OS,
    		where the default optimizations clashed.

		Preliminary IC910H support

- 20210507	Added serial port iterators for the IC-7610 and IC-R8600. Untested.

    		removing debug info we didn't need.

    		Adding /dev/ to discovered serial devices, also ignoring case on "auto"
    		for serial port device.

    		wfview's own memory system now tracks mode correctly. *however*, it
    		needs work:
   		It should track the selected filter, since this information is generally
    		available and useful, and it should also be storing the frequencies in Hz. 
		I am also not sure how well the stored memory mode specification
    		will work across multiple rigs.

    		Added more mode shortcuts: A = airband, $ = 4 meter band (shift-4), W =
    		WFM, V = 2M, U = 70cm, Shift-S = 23cm (S-band)

    		Fixed BSR mode and filter selection.

    		The band stacking register now uses the newer integer frequency parsing.
    		We also communicate a little slower to the serial rigs, which seems more
    		reliable.

    		Updater serialDeviceListCombo with current COM/tty port setting

    		pttyHandler doesn't use pty parameter in Linux so set to Q_UNUSED to eliminate warning


- 20210506	Force DTR/RTS to false to stop rigs going into TX if USB Send is enabled

    		Convert project to 32bit default on Windows and remove beginnings of VSPE code.


- 20210505	Add code to select virtual serial port

    		fixed a typo in mint instructions and ubuntu instructions

- 20210504	fixed the display instead of rigcaps so that ant sel starts with 1 instead of 0

    		Fix to add a blank user line in server if no users are configured.

    		small changes where to find the release; removed the src/build target directory requirement as the release unpacks in ./dist

- 20310503	Fix for Built-in audio on MacOS

- 20210501	Fixed bug 007, which allowed negative frequencies to be dialed.

    		Double-clicking the waterfall now obeys the tuning step and rounding
    		option in the settings.

    		Unified frequency-type MHz to use same code as Hz.

    		Add WFM mode for IC705 and remove duplicate WFM mode for IC-R8600

    		make bandType start at 0 so it doesn't overflow rigCaps.bsr

    		Fix windows release build

    		Changed the organization and domain to wfview and wfview.org.

    		Added more modes for the IC-R8600.

- 20210430	Different timing on command polling for serial rigs.


- 20210427	Minor changes to hide sat button until we have time to work on that
    		feature.

    		Additional bands added, Airband, WFM

    		added 630/2200m for 705; probably like the 7300 hardly usable for TX though. Also added auomatic switching to the view pane after non-BSR bands are selected

    		added 630/2200m to 7300/7610/785x

    		derp: fixed main dial freq display for non-BSR 4m band; see also previous commit

    		fixed main dial freq display for non-BSR bands 60, 630, 2200m if such a band was selected in the band select menu

    		Minor change to set frequency, which was lacking "emit" at the start.

    		changed the modeSelectCombo-addItem sequence a bit to have modes grouped; CW and CW-R are now grouped, as well as RTTY/RTTY-R; AM and FM are grouped now

- 20210426	Well, that was fun. Color preferences now work, and colors can be
    		modified from the preference file for custom plot colors.
    
    		The preference file now stores colors as unsigned int. To convert the
    		colors in the file to standard AARRGGBB format (AA = Alpha channel), use
    		python's hex() function. Maybe one day we will fix the qt bug and make
    		this save in a better format.

    		Added dynamic band buttons. Fixed multiple bugs related to various
    		differences in band stacking register addresses (for example, the GEN
    		band on the 705 has a different address from the 7100 and the 7300).
    		This code was only tested with the 9700.

    		started rough docs for the usermanual

- 20210425	More work on rigctld

		Faster polling. Clarified in comments around line 309 in wfmain.cpp.

    		Added ability to read RIT status. Added RIT to initial rig query. Added

    		Added ability to read RIT status. Added RIT to initial rig query. Added
    		variables to handle delayed command timing values. Fixed bug in
    		frequency parsing code that had existed for some time. Changed tic marks
    		on RIT knob because I wanted more of them. Bumped startup initial rig
    		state queries to 100ms. May consider faster queries on startup since we
    		are going to need more things queried on startup.

- 20210424	Receiver Incremental Tuning is in. The UI does not check the rig's
    		initial state yet, but the functions are partially in rigCommander for
    		that purpose.


- 20210423	Found two missing defaults in switch cases inside rigcommander.cpp.

		Modified rig power management to stop command ques (s-meter and others).
		Upon rig power up, the command queue is repurposed as a 3 second delay
		for bootup, and then, commands are issued to restore the spectrum
		display (if the wf checkbox was checked). I've made new functions,
		powerRigOff and powerRigOn, for these purposes.

    		work in progress on spectrum enable after power off/on.

		powerOff should work now.

- 20210420 	rigctl working (sort of) with WSJT-X

- 20210419	Initial commit of rigctld (doesn't currently do anything useful!)


- 20210417	Adding a default position for the frequency indicator line.

    		Goodbye tracer


- 20210416	added support info for prebuild-systems


- 20210412	added airband, dPMR, lw/mw European and fast HF/70/23cm 1 MHz tuning steps


- 20210411	Added ATU feature on 7610.

    		Now grabs scope state on startup. Found bug, did not fix, in the
    		frequency parsing code. Worked around it by using doubles for now.


- 20210410	Preamp and attenuator are now queried on startup. Additionally, the
    		preamp is checked when the attenuator is changed, and the attenuator is
    		checked with the preamp is changed. This helps the user understand if
    		the rig allows only one or the other to be active (such as the IC-9700).

    		Added frequency line. Let's see how we like it.

    		Add some preliminary parts of getting the attenuator, preamp, and
   		antenna selection on startup. UI not updated yet but getting there.


- 20210409	Moved ATU controls to main tab.

		Added waterfall theme combo box

    		Removed buttons from attenuator preamp UI controls.

    		Antenna selection might work, untested.

    		Preamp code is in. Can't read the current preamp state yet but we can
    		set it. Nicer names for preamp and attenuator menu items.


- 20210408    	Preamp data is now tracked (but not used yet)
		re-added lost tooptip for SQ slider


- 20210407 	Minor disconnect in the getDTCS call

    		Attenuators are in! Please try them out!


- 20210406 	The repeater setup now disables elements for things your rig doesn't do.

    		We now query the repeater-related things on startup, such that the
    		repeater UI is populated correctly.

 	   	We now have kHz as the assumed input format if there isn't a dot in the
    		entry box. Enjoy that!

    		Minor change so that we track the selected tone to mode changes, keeping
    		the radio in sync with the UI.

    		Tone, Tone Squelch, and D(T)CS seem to work as expected. Mode can be
    		selected.


- 20210405	removed 150 Hz CTCSS / NATO as it can't make that by itself

    		added 77.0 Hz tone to CTCSS

    		We can now read the repeater access mode and update the UI. What remains
    		is to be able to set the mode.

    		Working read/write of Tone, TSQL, and DTCS tones/code. Some code is also
    		present now to change the mode being used (tone, tsql, DTCS, or some
    		combo of the two).


- 20210404	Tone, TSQL, and DTCS code added, but not complete.

    		better tone mode names

    		Started work on the tone mode interface.


- 20210401	Added placeholders for attenuator, preamp, and antenna selection UI
    		controls.

    		Moved some repeater-related things out from rig commander and into a
    		shared header.


- 20210331	Adjusting signals and slots for repeater duplex.

    		Basic duplex code copied from wfmain to the new repeatersetup window.


- 20210330	Added conditional to debug on serial data write size.


- 20210329	Fix crash when radio is shutdown while wfview is connected.


- 20210311      Add local volume control for UDP connections.

                add volume control to audiohandler

                Small fixes to UDP server

                Add USB audio handling to UDP server

                Changed frequency parameters to (mostly) unsigned 64-bit ints. This
                makes all the rounding code very simple and removes many annoying lines
                of code designed to handle errors induced by using doubles for the
                frequency.

- 20210310	audio resampling added / opus

		updates on virtual serial port 

		fixed input combo boxes from growing

		fixed 4:15 rollover/disconnect issue		

- 20210304	Tuning steps! Still need to zero out those lower digits while scrolling
    		if box is checked.

    		Fix spectrum peaks as well.

		Fix spectrum 'flattening' with strong signal

		Add separate mutex for udp/buffers.

		supported rigs IC705, IC7300 (over USB, no sound), IC7610, IC785x

- 20210227	changed the way udp audio traffic is handled
                on both the TX and RX side.

- 20210226	Fixed minor bug where flock did not stop double-clicking on the
    		spectrum.

                S-meter ballistics:
                Turned up the speed. Once you see fast meters, there's no going back.
		Tested at 5ms without any issues, committing at 10ms for now. Note that
    		50ms in the first 'fixed' meter code is the same as 25ms now due to how
    		the command queue is structured. So 10ms is only a bit faster than
    		before.
                Fixed meter polling issue

- 20210224      fixed 785x lockup
                Added scale and dampening/averaging to s-meter

- 20210222      f-lock works now

- 20210221	added working s-meter
		added working power meter
		added visual studio 2019 solution (windows builds)
		changed packet handling, WIP
		rigtype stays in view on status bar
		added audio input gain slider

- 20210218
		added SQ slider
		added TX power slider
		added TX button
		added simplex/duplex/auto shifts
		added debug logging
		added TX and RX codec selections
		added RX audio buffer size slider
		started adding server setup
		
- 20210210	added control over LAN for rigs that support it
                has been tested on the 705, 7610, 785x, 9700.
		should work on 7700, 7800, other rigs.
		added different sampling rates for rx
		added different codecs
		added scope data for 7610, 785x.  
		several cosmetic changes in the UI
		fixed a slew of bugs
		builds on MAC-OS, Linux, Windows
		added ToFixed
		implemented connect/disconnect rig
		improved USB serial code
		fixed memory leak 
		redesigned rx audio path for better performance
		added round trip time for network connected devices
		added detection code of rigtype
		automatic detection of CIV control address
		added logfile capability
		works on a raspberry pi (USB and ethernet)


			

            

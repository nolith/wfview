#include "logcategories.h"

Q_LOGGING_CATEGORY(logSystem, "system")
Q_LOGGING_CATEGORY(logSerial, "serial")
Q_LOGGING_CATEGORY(logGui, "gui")
Q_LOGGING_CATEGORY(logRig, "rig")
Q_LOGGING_CATEGORY(logAudio, "audio")
Q_LOGGING_CATEGORY(logUdp, "udp")
Q_LOGGING_CATEGORY(logUdpServer, "udp.server")
Q_LOGGING_CATEGORY(logRigCtlD, "rigctld")
Q_LOGGING_CATEGORY(logTcpServer, "tcpserver")
Q_LOGGING_CATEGORY(logAudioConverter, "audioconverter")
